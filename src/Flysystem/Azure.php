<?php

namespace Drupal\flysystem_azure\Flysystem;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flysystem\Plugin\FlysystemPluginInterface;
use Drupal\flysystem\Plugin\FlysystemUrlTrait;
use Drupal\flysystem\Plugin\ImageStyleGenerationTrait;
use Drupal\flysystem_azure\Flysystem\Adapter\AzureBlobStorageAdapter;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal plugin for the "Azure" Flysystem adapter.
 *
 * @Adapter(id = "azure")
 */
class Azure implements FlysystemPluginInterface, ContainerFactoryPluginInterface {

  use ImageStyleGenerationTrait;
  use FlysystemUrlTrait {
    getExternalUrl as getDownloadlUrl;
  }

  /**
   * Plugin configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * The Client proxy.
   *
   * @var \MicrosoftAzure\Storage\Blob\BlobRestProxy
   */
  protected BlobRestProxy $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs an Azure object.
   *
   * @param array $configuration
   *   Plugin configuration array.
   */
  public function __construct(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration);
    $instance->logger = $container->get('logger.factory')->get('flysystem_azure');
    return $instance;
  }

  /**
   * Gets the Azure client.
   *
   * @return \MicrosoftAzure\Storage\Blob\BlobRestProxy
   *   The blob rest proxy.
   */
  public function getClient(): BlobRestProxy {
    if (!isset($this->client)) {
      $this->client = BlobRestProxy::createBlobService($this->getConnectionString());
    }

    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdapter(): AzureBlobStorageAdapter {
    try {
      return new AzureBlobStorageAdapter($this->getClient(), $this->configuration['container']);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());

      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl($uri): string {
    $target = $this->getTarget($uri);

    if (str_starts_with($target, 'styles/') && !file_exists($uri)) {
      $this->generateImageStyle($target);
    }

    return sprintf('%s/%s', $this->calculateUrlPrefix(), UrlHelper::encodePath($target));
  }

  /**
   * {@inheritdoc}
   */
  public function ensure($force = FALSE): array {
    try {
      $this->getAdapter()->listContents();
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return [];
  }

  /**
   * Gets the connection string.
   *
   * @return string
   *   The connection string.
   */
  protected function getConnectionString(): string {
    return "DefaultEndpointsProtocol=" . $this->configuration['protocol'] . ";AccountName=" . $this->configuration['name'] . ";AccountKey=" .
      $this->configuration['key'] . ";EndpointSuffix=" . $this->configuration['endpointSuffix'] . ";";
  }

  /**
   * Calculates the URL prefix.
   *
   * @return string
   *   The URL prefix in the form
   *   protocol://[name].blob.[endpointSuffix]/[container].
   */
  protected function calculateUrlPrefix(): string {
    return $this->configuration['protocol'] . '://' . $this->configuration['name'] . '.blob.' .
      $this->configuration['endpointSuffix'] . '/' . $this->configuration['container'];
  }

}
