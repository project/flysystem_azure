Flysystem Azure
===============

For setup instructions see the Flysystem README.md.

## CONFIGURATION ##

You can get all the configuration variables you need from Azure:
- If you go into the storage account you want to use and then Settings -> Access keys
- Click the show keys button
- Make a note of the DefaultEndpointsProtocol, AccountName, AccountKey and EndpointSuffix in the Connection string (you can also get the Account key from the Key above this)
- Make a note of the container name that you want media to be uploaded to from Blob service -> Containers in the storage account

When you have these, add the below to your `web/sites/default/settings.php` file:

```php
$schemes = [
  'azure' => [
    'driver' => 'azure',
    'config' => [
      'name' => '[your account name]',
      'key' => '[your account key]',
      'container' => '[your container name]',
      'endpointSuffix' => '[your endpoint suffix]',
      'protocol' => '[your default endpoints protocol]',
    ],

    'cache' => TRUE,
  ],
];

$settings['flysystem'] = $schemes;
```

To support serving JS and CSS settings, add `serve_js => TRUE` and / or `serve_css => TRUE` depending on your requirements to the `azure` key in the `$schemes` array.
